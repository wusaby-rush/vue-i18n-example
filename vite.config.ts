import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

// trans here for future use, it give convert from v-trans="key" to v-text="$t('key')"
export function vTransPlugin() {
  return {
    name: "v-trans",
    transform(code, id) {
      if (/\.vue$/.test(id)) {
        code = code.replace(
          /(?<!\\)v-trans=([\'\"])([^\'\"]*)([\'\"])/g,
          (match, p1, p2, p3) => {
            let key = p2.startsWith("$t(")
              ? p2.substring(3, p2.length - 2)
              : p2;
            return `v-text="$t('${key}')"`;
          }
        );
        code = code.replace(
          /(?<!\\)\{\{\s*\$t\(['"]([^'"]*)['"]\)\s*\}\}/g,
          "<span v-text=\"$t('$1')\"></span>"
        );
      }
      return {
        code,
        map: null,
      };
    },
  };
}

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vTransPlugin(), vue()],
});
