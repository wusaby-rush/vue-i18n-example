# vue-i18n example

## install

```sh
npm i vue-i18n
```

## configure chage locales

see `main.ts` and `App.vue`

## add and use locales

define your locales as json keys and sub keys:

```json
{
  "hello": "أهلاً!",
  "welcome": "مرحباً بك في تطبيق Vue.js الخاص بي."
}
```

now in your component use defined keys in `$t` function:

```vue
<p> {{ $t("welcome") }}
```

see `locales/` and `HelloWorld.vue` for example

## messages reporting

```sh
npx vue-i18n-extract report --vueFiles './src/**/*.vue' --languageFiles './src/locales/*.json'
```
